<?php

/**
 * Page callback for Hooker's main admin page.
 */
function hooker_admin_form() {
  $path = drupal_get_path('module', 'hooker') .'/';
  $form = array();
  $form['#tree'] = TRUE;
  foreach (_hooker_hooks(TRUE) as $hook => $data) {
    $form['hooks'][$hook]['edit'] = array(
      '#type' => 'image_button',
      '#title' => t('Edit hook'),
      '#src' => $path .'text-editor.png',
      '#submit' => array('hooker_admin_edit_button'),
      '#hook' => $hook,
    );
    $form['hooks'][$hook]['delete'] = array(
      '#type' => 'image_button',
      '#title' => t('Delete hook'),
      '#src' => $path .'edit-delete.png',
      '#submit' => array('hooker_admin_delete_button'),
      '#hook' => $hook,
    );
  }
  return $form;
}

/**
 * Edit button callback for the hook listing form.
 */
function hooker_admin_edit_button($form, &$form_state) {
  $button = $form_state['clicked_button'];
  $hook = $button['#hook'];
  $form_state['redirect'] = 'admin/settings/hooker/edit/'. $hook;
}

/**
 * Delete button callback for the hook listing form.
 * Redirects the user to the confirmation form.
 */
function hooker_admin_delete_button($form, &$form_state) {
  $button = $form_state['clicked_button'];
  $hook = $button['#hook'];
  $form_state['redirect'] = 'admin/settings/hooker/delete/'. $hook;
}

/**
 * Theme function for the Hooker admin overview form.
 */
function theme_hooker_admin_form(&$form) {
  $rows = array();
  if (!empty($form['hooks'])) {
    foreach (element_children($form['hooks']) as $hook) {
      $row = array();
      $row[] = check_plain('hook_'. $hook);
      $row[] = l(t('API documentation'), 'http://api.drupal.org/api/function/hook_'. $hook, array('absolute' => TRUE));
      $row[] = drupal_render($form['hooks'][$hook]);
      $rows[] = $row;
    }
  }

  $link = l(t('Click here'), 'admin/settings/hooker/add');
  $row = array();
  if (empty($rows)) {
    $row[] = array(
      'data' => t('No hooks have been added. !url to create one.', array('!url' => $link)),
      'colspan' => 3,
    );
  }
  else {
    $row[] = array(
      'data' => t('!url to add a new hook implementation.', array('!url' => $link)),
      'colspan' => 3,
    );
  }
  $rows[] = $row;

  $link = l('api.drupal.org', 'http://api.drupal.org/api/group/hooks', array('absolute' => TRUE));
  $output = '<p>'. t("Use Hooker to execute snippets of PHP when Drupal hooks are triggered. See !url for more information about Drupal hooks.", array('!url' => $link)) .'</p>';
  $output .= theme('table', array(), $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Constructor for the hook edit form.
 */
function hooker_edit($form_state, $hookname = NULL) {
  $hooks = _hooker_hooks(TRUE);
  if (isset($hookname)) {
    if (!empty($hooks[$hookname])) {
      $hook = $hooks[$hookname];
      $form['hook'] = array(
        '#type' => 'value',
        '#value' => $hookname,
      );
      $link = l('hook_'. $hookname, 'http://api.drupal.org/api/group/hooks', array('absolute' => TRUE));
      $form['hookname'] = array(
        '#type' => 'item',
        '#title' => t('Hook'),
        '#value' => $link,
      );
    }
    else {
      $form['hook'] = array(
        '#type' => 'textfield',
        '#title' => t('Hook'),
        '#default_value' => $hookname,
        '#required' => TRUE,
      );
      $hook = new stdClass();
      $hook->hook = $hookname;
      $hook->params = '';
      $hook->code = '';
    }
  }
  else {
    $form['hook'] = array(
      '#type' => 'textfield',
      '#title' => t('Hook name'),
      '#default_value' => '',
      '#required' => TRUE,
    );
    $form['new_hook'] = array(
      '#type' => 'value',
      '#value' => TRUE,
    );
    $hook = new stdClass();
    $hook->hook = '';
    $hook->params = '';
    $hook->code = '';
  }

  $form['params'] = array(
    '#type' => 'textfield',
    '#title' => t('Function parameters'),
    '#default_value' => $hook->params,
    '#description' => t('If the hoook in question requires parameters, they must be entered here just as they would appear in a normal module implementation.'),
    '#required' => FALSE,
  );

  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('PHP code'),
    '#rows' => 20,
    '#default_value' => $hook->code,
    '#required' => TRUE,
  );

  $form['buttons']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('hooker_edit_save'),
  );

  if (!empty($hooks[$hookname])) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('hooker_admin_delete_button'),
      '#hook' => $hookname,
    );
  }

  return $form;
}

/**
 * Validation callback for the hook edit form.
 */
function hooker_edit_validate($form, &$form_state) {
  $values = $form_state['values'];
  $hooks = _hooker_hooks();
  if (!empty($values['new_hook'])) {
    if (isset($hooks[$values['hook']])) {
      form_set_error('hook', t('The hook %hookname is already defined.', array('%hookname' => $values['hook'])));
    }
  }
}

/**
 * Submit button callback for the hook edit form.
 */
function hooker_edit_save($form, &$form_state) {
  if (empty($form_state['values']['new_hook'])) {
    drupal_write_record('hooker', $form_state['values'], array('hook'));
  }
  else {
    drupal_write_record('hooker', $form_state['values']);
  }

  drupal_set_message('Your hook was saved.');
  $form_state['redirect'] = 'admin/settings/hooker';
  _hooker_hooks(TRUE);
}


/**
 * Menu callback -- ask for confirmation of rule deletion
 */
function hooker_delete_confirm(&$form_state, $hook) {
  $form['hook'] = array(
    '#type' => 'value',
    '#value' => $hook,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => 'hook_'. $hook)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/hooker',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute node deletion
 */
function hooker_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query("DELETE FROM {hooker} WHERE hook = '%s'", $form_state['values']['hook']);
  }
  $form_state['redirect'] = 'admin/settings/hooker';
  _hooker_hooks(TRUE);
  return;
}
