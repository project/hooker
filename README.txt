
ABOUT
=====
Hooker allows an administrator to enter snippets of code via the web interface. These snippets are then magically turned into Drupal hook implementations. Have you ever wanted to keep mission-critical business logic in a database table called 'hooker'? If so, this module is for you.

USAGE
=====
After installing the module, navigate to the Hooker page at admin/settings/hooker. There, you can edit existing Hooker records and create new ones to implent additional Drupal hooks. If the hook in question requires parameters, they must be entered into the 'Parameters' field, just as they would appear in a normal module.

In all seriousness, this module was written on a dare. It should not -- repeat, NOT -- be used on a production site. Instead, a custom module should be written that actually implements hooks. It is, however, an interesting exercise in dynamically generating hook implementations.
